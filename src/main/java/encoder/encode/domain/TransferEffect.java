package encoder.encode.domain;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.Mat;

import encoder.encode.domain.EffectOverlayImage;

/**
 * Created by htsuji on 2017/12/11.
 * 透過合成エフェクトクラス
 */
public class TransferEffect implements EffectOverlayImage{
    //透過合成
	private double mSrcWeight;
	private double mEffectWeight;
	private double mAllWeight;

	public TransferEffect(double alpha, double beta, double gamma) {
		mSrcWeight = alpha;
		mEffectWeight = beta;
		mAllWeight = gamma;
	}

	//テストを書きたい！
	public void overlay(Mat srcMat, Mat effectMat, Mat masked) {
        //透過合成
		Mat distMat = new Mat();
        opencv_core.addWeighted(effectMat, mSrcWeight, srcMat, mEffectWeight, mAllWeight, distMat);
        distMat.copyTo(srcMat);
		distMat.close();
	}
}
