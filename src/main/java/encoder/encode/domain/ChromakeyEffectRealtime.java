package encoder.encode.domain;
import static org.bytedeco.javacpp.opencv_imgproc.*;

import org.bytedeco.javacpp.indexer.UByteIndexer;
import org.bytedeco.javacpp.opencv_core.Mat;

/**
 * Created by htsuji on 2017/12/11.
 * クロマキー合成エフェクトクラス。マスク動画作成用
 */
public class ChromakeyEffectRealtime extends ChromakeyEffect implements EffectMask{

	//メンバー変数
	//指定色
	private int [] mColors;
	//ローカル変数
    private final int[] BGR_WHITE = {255, 255, 255};
    private final int COLOR_FROM = COLOR_BGRA2GRAY;

    public ChromakeyEffectRealtime(int rgbcolors[], int thresholdValue, int maxValue, int thresholdType) {
    	super(thresholdValue, maxValue, thresholdType);
		mColors = new int [3];
		mColors = rgbcolors;
	}

    @Override
	public Mat getMask(Mat effectMat) {
        //指定した色以外の範囲を白抜き
        Mat discolorMat = getDiscoloration(effectMat, mColors, super.mThresholdValue);

        //グレースケール作成 RGBのみサポート
        Mat gray=new Mat();
    	cvtColor(discolorMat, gray, COLOR_FROM);
        discolorMat.close();
        return gray;
	}

	//白抜き画像作成
    private Mat getDiscoloration(Mat srcMat, int [] colors, int threshold) {
		int[] values = new int[3];
    	//alphaが使えるならアルファブレンドも試してみたい…

		//matはコピーしておく
		Mat retMat = srcMat.clone();

    	//Matの生データをなめてWHITEで埋める。多分重めの処理になると思うので、処理した動画ファイルを作るといいかも
    	UByteIndexer srcIndexer =retMat.createIndexer();
    	for (int x = 0; x < srcIndexer.rows(); x++) {
    		for (int y = 0; y < srcIndexer.cols(); y++) {
    			srcIndexer.get(x, y, values);
				//閾値範囲外？
    			if ( !is_this_in_area(values, colors, threshold) ){
    				//白抜き
    				srcIndexer.put(x, y, BGR_WHITE);
    			}
    		}
    	}

    	return retMat;
    }
	//閾値範囲内？
    private boolean is_this_in_area(int [] thiscolors, int [] areacolor, int threshold) {
		if ( (areacolor[0]-threshold < thiscolors[0] &&  thiscolors[0] < areacolor[0] + threshold)
			&& (areacolor[1]-threshold < thiscolors[1] &&  thiscolors[1] < areacolor[1] + threshold)
			&& (areacolor[2]-threshold < thiscolors[2] &&  thiscolors[2] < areacolor[2] + threshold) ){
			return true;
		}
		return false;
    }
}
