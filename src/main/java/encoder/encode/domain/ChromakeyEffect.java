package encoder.encode.domain;
import static org.bytedeco.javacpp.opencv_imgproc.*;

import org.bytedeco.javacpp.opencv_core.Mat;

import encoder.encode.domain.EffectOverlayImage;

/**
 * Created by htsuji on 2017/12/28.
 * クロマキー合成エフェクトクラス
 */
public class ChromakeyEffect implements EffectOverlayImage{

	//メンバー変数
	//色抜きで使用する閾値
	protected int mThresholdValue;
	//グレースケール指定時のパラメータ
	protected int mMaxValue;
	protected int mThresholdType;

    public ChromakeyEffect(int thresholdValue, int maxValue, int thresholdType) {
		mThresholdValue = thresholdValue;
		mMaxValue = maxValue;
		mThresholdType = thresholdType;
	}

	@Override
	public void overlay(Mat srcMat, Mat effectMat, Mat gray) {
    	//output処理
    	Mat retMat = new Mat();
        threshold(gray, retMat, mThresholdValue, mMaxValue, mThresholdType );
    	effectMat.copyTo(srcMat, retMat);
        retMat.close();
	}
}
