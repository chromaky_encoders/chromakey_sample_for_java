package encoder.encode.domain;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;

import encoder.encode.domain.usecase.EffectAppliedMovieUsecase;
import encoder.encode.domain.usecase.EffectEncodeUsecase;

/**
 * Created by htsuji on 2017/12/24.
 * 動画に対してエフェクトを適用するクラス
 */
public class EffectAppliedMovieUsecaseImpl extends EffectAppliedMovieUsecase{

	private EffectAppliedMovieRunnable mRunnable=null;

	public EffectAppliedMovieUsecaseImpl(String inputFile, EffectEncodeUsecase encoder) throws Exception {
		super(inputFile,encoder);
		//元動画読み込み
		mRunnable = new EffectAppliedMovieRunnable(inputFile, encoder);
	}

	@Override
	public void release() throws Exception {
		//動画のリリースして終わり
		//2重呼び出しで死なないように
		if(mRunnable!=null) {
			mRunnable.stop();
			mRunnable = null;
		}
	}

	@Override
	public void start(int startFrameNo, int x, int y, int width, int height, EffectAppliedProgress progressCallback) {
		//2重呼び出しで死なないように
		if(mRunnable != null)
			mRunnable.start(startFrameNo, x, y, width, height, progressCallback);
	}

	@Override
	public FFmpegFrameGrabber getSrcGrabber() {
		FFmpegFrameGrabber grabbe = null;

		//2重呼び出しで死なないように
		if(mRunnable != null) {
			grabbe = mRunnable.getSrcGrabber();
		}

		return grabbe;
	}

	/**
	 * Created by htsuji on 2017/12/24.
	 * 実際にエフェクトを適用するスレッドクラス
	 */
	private class EffectAppliedMovieRunnable implements Runnable {
		private int mX;
		private int mY;
		private int mWidth;
		private int mHeight;
		private int mStartFrameNo;
		private EffectAppliedProgress mProgressCallback=null;
		private FFmpegFrameGrabber mInputGrabber=null;
		private EffectEncodeUsecase mEncoder=null;

		private boolean runningThread=false;
		//内部でthreadを立ち上げて処理
	    private Thread thread=null;

		public EffectAppliedMovieRunnable(String inputFile, EffectEncodeUsecase encoder) throws Exception {
			mInputGrabber = new FFmpegFrameGrabber(inputFile);
			mInputGrabber.start();

			mEncoder = encoder;

			//動画のloadまでやっておく
			mEncoder.load();
			thread = new Thread(this);
		}

		//動画ベースの反映
		public void start(int startFrameNo, int x, int y, int width, int height, EffectAppliedProgress progressCallback) {
			mX = x;
			mY = y;
			mWidth = width;
			mHeight = height;
			mStartFrameNo = startFrameNo;
			mProgressCallback = progressCallback;
            runningThread = true;
			thread.start();
		}

		public void stop() {
			runningThread = false;
		}

		public FFmpegFrameGrabber getSrcGrabber() {
			return mInputGrabber;
		}

        @Override
        public void run() {
            Frame srcFrame = null;
            Frame mergeFrame = null;
//            boolean mergeStart = false;

            /* ffmpeg_audio encoding loop */
    		try {
                //エンコード開始前によぶ
                mProgressCallback.onStart();

                while (runningThread && ((srcFrame = mInputGrabber.grab()) != null)) {

                	//エフェクト適用範囲: 適用したい場所の前 or 適用し終えた後effect長さ分読み終わった場合
                	if( (mStartFrameNo <= mInputGrabber.getFrameNumber())
                		&&
                		((srcFrame.image!=null && !mEncoder.isAtVideoEndpoint())
                		|| (srcFrame.samples!=null && !mEncoder.isAtAudioEndpoint())) ) {
                		mergeFrame = mEncoder.encodeFrame(srcFrame, mX, mY, mWidth, mHeight);
                		mProgressCallback.onFrame(mergeFrame);
                		mEncoder.releaseFrame(mergeFrame);
                	} else { //前にちゃんとeffectが取れた時だけ次を取得
                		mProgressCallback.onFrame(srcFrame);
                	}
    			}
                //終わったので通知して終了
    			mProgressCallback.onFinished();
    		} catch (Exception e) {
    			e.printStackTrace();
    			mProgressCallback.onError();
    		} finally {
    			try {
	    			mInputGrabber.stop();
	    			mInputGrabber.release();
	    			mEncoder.release();
    			} catch(Exception e) {
        			e.printStackTrace();
        			//close失敗時は何をすればいいんだろう？
    			} finally {
	    			//cancel時
	    			if(!runningThread) {
	    				mProgressCallback.onCanceled();
	    			}
    			}
    		}
    	}
	}
}
