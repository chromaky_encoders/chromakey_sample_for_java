package encoder.encode.domain;

import org.bytedeco.javacpp.opencv_core.Mat;

/**
 * Created by htsuji on 2017/12/11.
 * エフェクト合成処理用インターフェイスクラス
 * 重ねる手段ごとに継承したクラスの処理を実装する。
 */
public interface EffectOverlayImage{

	/***
     * フレーム重ね合わせ処理
     * @param srcMat         元フレーム(Mat)
     * @param effectMat      重ねるフレーム(Mat)
     * @note 元のMatデータを触らないとoverlay出来ない
     */
	public void overlay(Mat srcMat, Mat effectMat, Mat masked);

}
