package encoder.encode.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import encoder.encode.domain.resource.EffectResourceRepository;
import encoder.encode.domain.usecase.EffectAppliedMovieUsecase;
import encoder.encode.domain.usecase.EffectEncodeUsecase;
import encoder.encode.domain.usecase.EffectManageUsecase;

/**
 * Created by htsuji on 2017/12/13.
 * EffectManageUsecase実装
 */
public class EffectManageUsecaseImpl implements EffectManageUsecase{

//	ArrayMap<String, EffectEncodeUsecase> mEncoderMap = new ArrayMap<>();
	//AndroidではArrayMapを使用。とりあえず普通のMapで。ダサい
	Map<String, EffectEncodeUsecase> mEncoder = new HashMap<String, EffectEncodeUsecase>();

	public EffectManageUsecaseImpl() {
		//エフェクト一覧を作成
		List<String> effectList = EffectResourceRepository.getAll();
		for( int i = 0 ; i < effectList.size() ; i++ ) {
			String effectName = effectList.get(i);

			//エフェクトに合わせたEffectEncodeUsecaseの生成
			String fpath = EffectResourceRepository.getPath(effectName);
			EffectOverlayImage image = EffectResourceRepository.getOverlayImage(effectName);
			EffectOverlayAudio audio = EffectResourceRepository.getOverlayAudio(effectName);
			EffectEncodeUsecase usecase;
			if( EffectResourceRepository.getMaskPath(effectName) != null ) {
				usecase = new EffectEncodeUsecaseImpl(fpath, EffectResourceRepository.getMaskPath(effectName), image, audio);
			} else if ( EffectResourceRepository.getImageMask(effectName) != null ) {
				usecase = new EffectEncodeUsecaseImpl(fpath, EffectResourceRepository.getImageMask(effectName), image, audio);
			} else {
				usecase = new EffectEncodeUsecaseImpl(fpath, image, audio);
			}
			mEncoder.put(effectName, usecase);
		}
	}

	@Override
	public List<String> getAll() {
		//エフェクト一覧取得
		return EffectResourceRepository.getAll();
	}

	@Override
	public EffectEncodeUsecase getEncoder(String name) {
		return mEncoder.get(name);
	}

	@Override
	public EffectAppliedMovieUsecase getUsecase(String inputFile, String effectName) throws Exception {
		EffectEncodeUsecase encoder = getEncoder(effectName);
		//usecase実体にある
		EffectAppliedMovieUsecase usecase = new EffectAppliedMovieUsecaseImpl(inputFile, encoder);
		return usecase;
	}
}
