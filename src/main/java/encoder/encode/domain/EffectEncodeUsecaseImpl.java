package encoder.encode.domain;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Rect;
import org.bytedeco.javacpp.opencv_core.Size;
import org.bytedeco.javacv.OpenCVFrameConverter.ToMat;

import encoder.encode.domain.usecase.EffectEncodeUsecase;
import encoder.encode.domain.usecase.EffectImageFrame;

import static org.bytedeco.javacpp.opencv_imgproc.resize;

//import org.bytedeco.javacpp.Pointer;
//import static org.bytedeco.javacpp.opencv_core.*;

/**
 * Created by htsuji on 2017/12/11.
 * エフェクトエンコード実装
 */
public class EffectEncodeUsecaseImpl implements EffectEncodeUsecase {

	private EffectOverlayImage mOverlayImage;
	private EffectOverlayAudio mOverlayAudio;
	private String mFile;
	//video用のgrabber
	private FFmpegFrameGrabber mGrabberVideo=null;
	//audio用のgrabber
	private FFmpegFrameGrabber mGrabberAudio=null;
	private String mMaskFile = null;
	//マスク動画を使わずにMaskをかけたい場合に使用
	private EffectMask mMaskIF = null;
	//video mask用のgrabber
	private FFmpegFrameGrabber mGrabberVideoMask=null;
	private ToMat mToMat=null;
	private boolean mIsAtEndpointVideo = false;
	private boolean mIsAtEndpointAudio = false;

	public EffectEncodeUsecaseImpl(String filePath, EffectOverlayImage imageSetting, EffectOverlayAudio audioSetting) {
		initial(filePath,imageSetting,audioSetting);
	}

	public EffectEncodeUsecaseImpl(String filePath, String maskPath, EffectOverlayImage imageSetting, EffectOverlayAudio audioSetting) {
		mMaskFile = maskPath;
		initial(filePath,imageSetting,audioSetting);
	}

	//リアルタイムでMaskをかけるとき限定の処理動画生成時限定処理
	public EffectEncodeUsecaseImpl(String filePath, EffectMask maskIF, EffectOverlayImage imageSetting, EffectOverlayAudio audioSetting) {
		mMaskIF = maskIF;
		initial(filePath,imageSetting,audioSetting);
	}

	private void initial(String filePath, EffectOverlayImage imageSetting, EffectOverlayAudio audioSetting) {
		mOverlayImage = imageSetting;
		mOverlayAudio = audioSetting;
		mFile = filePath;
		//ToMatは作っておく
		mToMat = new ToMat();

		mIsAtEndpointVideo = false;
		mIsAtEndpointAudio = false;
	}

	//動画ファイル読み込み
	public void load() throws Exception{
		//audio, videoそれぞれでframeとっておかないとわからない
		if(mGrabberVideo == null)
			mGrabberVideo = openGrabber(mFile);

		if(mGrabberAudio == null)
			mGrabberAudio = openGrabber(mFile);

		if(mGrabberVideoMask == null && mMaskFile != null)
			mGrabberVideoMask = openGrabber(mMaskFile);
	}

	private FFmpegFrameGrabber openGrabber(String file) throws Exception {
		FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(file);
		grabber.start();
		return grabber;
	}

	@Override
	public void release() throws Exception{
		if(mGrabberVideo != null) {
			releaseGrabber(mGrabberVideo);
			mGrabberVideo=null;
		}

		if(mGrabberAudio != null) {
			releaseGrabber(mGrabberAudio);
			mGrabberAudio=null;
		}

		if(mGrabberVideoMask != null) {
			releaseGrabber(mGrabberVideoMask);
			mGrabberVideoMask=null;
		}
	}

	private void releaseGrabber(FFmpegFrameGrabber grabber) throws Exception {
		grabber.stop();
		grabber.release();
	}

	//動画ファイルから1フレーム読み出してoverlay
	//distFrameは削除してね
	@Override
	public Frame encodeFrame(Frame srcFrame, int x, int y, int width, int height) throws Exception{

		Frame distFrame = null;
		EffectImageFrame effectImageFrame = null;
		Frame effectAudioFrame = null;

		if( srcFrame.image != null ) {//映像合成
			effectImageFrame = getNextEffectImageFrame();
			if(effectImageFrame != null) {
				distFrame = overlayImage(srcFrame, effectImageFrame, x, y, width, height);
				releaseImageFrame(effectImageFrame);
			} else {
				mIsAtEndpointVideo = true;
			}
		} else if( srcFrame.samples != null ) {//音声合成
			effectAudioFrame = getNextEffectAudioFrame();
			if(effectAudioFrame != null) {
				distFrame = overlayAudio(srcFrame, effectAudioFrame);
			} else {
				mIsAtEndpointAudio = true;
			}
		}

		//エフェクトマージしなかった場合はsrcをclone
		if(distFrame == null) {
			distFrame = srcFrame.clone();
		}
		return distFrame;
	}

	@Override
	public boolean isAtVideoEndpoint() {
		return mIsAtEndpointVideo;
	}

	@Override
	public boolean isAtAudioEndpoint() {
		return mIsAtEndpointAudio;
	}

	@Override
    public void releaseFrame(Frame frame) {
		if( frame.image != null ) mToMat.convert(frame).close();
	}

	@Override
    public String getEffectFile() {
		return mFile;
	}

	//ここから下はprivateに変えたい
	@Override
	public EffectImageFrame getNextEffectImageFrame() throws Exception{
		EffectImageFrame frame = new EffectImageFrame();
		frame.mFrame = mGrabberVideo.grabImage();
		if(frame.mFrame == null) {
			return null;
		}

		if (mGrabberVideoMask != null) {
			Frame maskFrame = mGrabberVideoMask.grabImage().clone();
			if(maskFrame != null) {
				frame.mMasked = mToMat.convert(maskFrame);
			}
			if(frame.mMasked == null ) {
				return null;
			}
		} else if(mMaskIF != null) {
			frame.mMasked = mMaskIF.getMask(mToMat.convert(frame.mFrame));
		} else {
			frame.mMasked = null;
		}
		return frame;
	}

	@Override
	public void releaseImageFrame(EffectImageFrame frame) {
//		frame.mFrame.close();
		if(frame.mMasked != null) {
			frame.mMasked.close();
		}
	}

	@Override
	public Frame getNextEffectAudioFrame() throws Exception{
		return mGrabberAudio.grabSamples();
	}

	//convertしてoverlayする
	@Override
	public Frame overlayImage(Frame srcFrame, EffectImageFrame effectFrame, int x, int y, int width, int height) {
		Mat srcMat = mToMat.convert(srcFrame).clone();
		Mat effectMat = mToMat.convert(effectFrame.mFrame);
		Mat effectResizeMat = new Mat();
		Mat maskResizeMat=null;

		//effectはサイズ変更する
		resize(effectMat, effectResizeMat, new Size(width, height));
		if(effectFrame.mMasked != null) {
			maskResizeMat = new Mat();
			resize(effectFrame.mMasked, maskResizeMat, new Size(width, height));
		}

		//srcはRoiを取得
        Rect roi = new Rect(x, y, effectResizeMat.cols(), effectResizeMat.rows());
        Mat settingMat = new Mat(srcMat, roi);

		//overlay
        mOverlayImage.overlay(settingMat, effectResizeMat, maskResizeMat);

        //この中で作ったMatは消しておく。(src, effect除く)
        effectResizeMat.close();
        if(maskResizeMat != null) {
        	maskResizeMat.close();
        }
        settingMat.close();
        roi.close();

        return mToMat.convert(srcMat);
	}

	//音声のoverlay
	@Override
	public Frame overlayAudio(Frame srcFrame, Frame effectFrame) {
		//リサイズはいらないのでそのままoverlayを呼ぶ
		return mOverlayAudio.overlay(srcFrame, effectFrame);
	}

}
