package encoder.encode.domain;
import static org.bytedeco.javacpp.opencv_imgproc.*;

import org.bytedeco.javacpp.indexer.UByteIndexer;
import org.bytedeco.javacpp.opencv_core.Mat;

//import encoder.encode.domain.EffectOverlayImage;

/**
 * Created by htsuji on 2017/12/11.
 * カラーキー合成エフェクトクラス マスク動画作成用
 */
public class ColorkeyEffectRealtime extends ChromakeyEffect implements EffectMask{

	//メンバー変数
	//指定色(輝度)
	private int mLuminance;

	//ローカル変数
	private final int LUMINANCE_WHITE = 255;    
    private final int COLOR_FROM = COLOR_RGB2HSV;
    private final int LUMINANCE_INDEX = 2;

    public ColorkeyEffectRealtime(int luminance, int thresholdValue, int maxValue, int thresholdType) {
    	super(thresholdValue, maxValue, thresholdType);
    	mLuminance = luminance;
	}

    @Override
	public Mat getMask(Mat effectMat) {
        //指定した色以外の範囲を白抜き
		return getGrayMask(effectMat, mLuminance, super.mThresholdValue);
	}

	//白抜き画像作成
    private Mat getGrayMask(Mat srcMat, int luminance, int threshold) {
        Mat discolorMat = getDiscoloration(srcMat, luminance, threshold);
        //グレースケール作成
        Mat gray = getGray(discolorMat);
        discolorMat.close();
        return gray;
    }

	//白抜き画像作成
    private Mat getDiscoloration(Mat srcMat, int luminance, int threshold) {
		int[] values = new int[3];

		//matはコピーして色変換
		Mat retMat = new Mat();
		cvtColor(srcMat, retMat, COLOR_FROM);

    	//Matの生データをなめてWHITEで埋める。多分重めの処理になると思うので、処理した動画ファイルを作るといいかも
    	UByteIndexer srcIndexer =retMat.createIndexer();
    	for (int x = 0; x < srcIndexer.rows(); x++) {
    		for (int y = 0; y < srcIndexer.cols(); y++) {
    			srcIndexer.get(x, y, values);
				//閾値範囲外？
    			if ( (values[LUMINANCE_INDEX] <= luminance-threshold) || (luminance + threshold <= values[LUMINANCE_INDEX]) ) {
    				//白抜き
    				values[LUMINANCE_INDEX] = LUMINANCE_WHITE;
    				srcIndexer.put(x, y, values);
    			}
    		}
    	}

    	return retMat;
    }

	//グレースケール
    private Mat getGray(Mat srcMat) {
    	Mat retMat = new Mat();

    	//2度の変換が必要
    	cvtColor(srcMat, retMat, COLOR_HSV2RGB);
    	cvtColor(retMat, retMat, COLOR_RGB2GRAY);
    	return retMat;
    }

}
