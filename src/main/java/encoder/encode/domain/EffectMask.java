package encoder.encode.domain;

import org.bytedeco.javacpp.opencv_core.Mat;

/**
 * Created by htsuji on 2017/12/28.
 * 実機では使用しない予定。Mask動画作成用IF
 */
public interface EffectMask {
	public Mat getMask(Mat effectMat);
}
