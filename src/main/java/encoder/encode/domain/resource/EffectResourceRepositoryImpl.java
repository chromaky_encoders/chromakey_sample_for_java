package encoder.encode.domain.resource;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import encoder.encode.domain.ChromakeyEffect;
import encoder.encode.domain.ChromakeyEffectRealtime;
import encoder.encode.domain.ColorkeyEffectRealtime;
import encoder.encode.domain.EffectMask;
import encoder.encode.domain.EffectOverlayAudio;
import encoder.encode.domain.EffectOverlayImage;
import encoder.encode.domain.TransferEffect;

import static org.bytedeco.javacpp.opencv_imgproc.*;

/**
 * Created by htsuji on 2017/12/13.
 * エフェクト関連情報を管理するためのクラス実装
 * domainのモジュールが使用することを想定
 * 使い勝手を考慮してstatic定義
 *  staticメソッドのinterface定義ができないので、implements EffectResourceInterface定義はしていない
 */
public class EffectResourceRepositoryImpl implements EffectResourceInterface{


	public static final String TOPDIR="D:\\data\\testdata\\win\\effect_test";
	public static final String effectList[] = {"syuwa","fire"};

	//mapにしたいが、定義が面倒なのでリスト
	private List<DataList> mSettingList;
	private List<String> mNameList;
	private EffectOverlayAudio mAudio;

	public class DataList {
		EffectOverlayImage mImage=null;
		String mFile=null;
		EffectMask mMaskIF=null;
		String mMaskFile=null;

		public DataList(EffectOverlayImage image, String file, EffectMask maskif) {
			mImage = image;
			mFile = file;
			mMaskIF = maskif;
		}

		public DataList(String file, String maskFile, EffectOverlayImage image) {
			mImage = image;
			mFile = file;
			mMaskFile = maskFile;
		}
	}

	//infra等のAPIを使用して、エフェクトに関する情報を構築する。
	public EffectResourceRepositoryImpl() {
		//とりあえずテスト用にMainで作成した一覧を
		mSettingList = new ArrayList<DataList>();

		//★エフェクトの識別子が欲しい。検索にも利用する
		mNameList = new ArrayList<String>();

		//★エフェクトを重ねるときの手段に関する種別
		//　今のところ透過、クロマキー、カラーキー
		//　(色空間HSVとYUVで複数あるといいかも。効果は確認できていない)
		//★各種別で欲しい設定は関数内参照
		initTransfer(mSettingList, mNameList);
//		initChromakey(mSettingList, mNameList);
//		initColorkey(mSettingList, mNameList);
//		initColorkeyYUV(settingList, mNameList);
		mAudio = new EffectOverlayAudio();
	}


	/**
     *  エフェクト一覧取得
     *  @return エフェクト名一覧
     */
	public List<String> getAll() {
		return mNameList;
	}

	/**
     *  エフェクトパス取得
     *  @param エフェクト名
     *  @return パス
     */
	public String getPath(String name) {
		int index = mNameList.indexOf(name);
		return mSettingList.get(index).mFile;
	}

	/**
     *  エフェクトイメージクラス取得
     *  @param エフェクト名
     *  @return エフェクトイメージクラス
     *  @note イメージがない場合はnullが返る
     */
	public EffectOverlayImage getOverlayImage(String name) {
		int index = mNameList.indexOf(name);
		return mSettingList.get(index).mImage;
	}

	/**
     *  エフェクト音声クラス取得
     *  @param エフェクト名
     *  @return エフェクト音声クラス
     *  @note 音声がない場合はnullが返る
     */
	public EffectOverlayAudio getOverlayAudio(String name) {
		return mAudio;
	}

	public void initTransfer(List<DataList> settingList, List<String> nameList) {

		double [][] alpha_beta_gamma = {
				{0.75,1,0},
				{1,1,0},
				{0.5,1,1},
				{0.5,1,2},
				};

		for (int i = 0 ; i < effectList.length; i ++) {
			for(int j = 0 ; j < alpha_beta_gamma.length; j ++) {
				String name;
				String dir;
				TransferEffect effect;
				double alpha, beta, gamma;
		
				alpha=alpha_beta_gamma[j][0];
				beta=alpha_beta_gamma[j][1];
				gamma=alpha_beta_gamma[j][2];
				name = effectList[i] + "_transfer_"+alpha+"_"+beta+"_"+gamma;
				nameList.add(name);

				dir = TOPDIR + "\\"+effectList[i]+".mp4";

				//★透過合成はalpha, beta, gammaというパラメータが欲しいです。
				//  OpenCVのaddWeightedで使用
				effect = new TransferEffect(alpha, beta, gamma);
				settingList.add(new DataList(effect, dir, null));
			}
		}
	}

	public void initChromakey(List<DataList> settingList, List<String> nameList) {

		for (int index = 0 ; index < effectList.length; index ++) {
			int ttypeList[] = {THRESH_BINARY,THRESH_TOZERO};
			int [] colors = {0,0,0};//black
			int threshold_step = 25;
			int threshold_start = 50;
			int threshold_max = 150;
			int threshold_max_start = 50;
			int threshold_max_step = 50;
			String dir = TOPDIR + "\\"+effectList[index]+".mp4";
			String mask = TOPDIR + "\\"+effectList[index]+"_chromakey.mp4";

			File file = new File(mask);

			if(file.exists()) {
				String name = effectList[index] + "_with_mask";
				nameList.add(name);
				ChromakeyEffect chromakey = new ChromakeyEffect(125, 50, THRESH_BINARY);
				settingList.add(new DataList(dir, mask, chromakey));
			}

			for (int i = 0 ; i < ttypeList.length ; i ++) {
				for (int max = threshold_max_start ; max < threshold_max ; max += threshold_max_step ) {
					for (int threshold = threshold_start ; threshold < threshold_max ; threshold += threshold_step) {
						String name = effectList[index] + "_chromakey_" + getStringFromType(ttypeList[i])+"_max_"+ max +"_threshold_"+threshold;
						nameList.add(name);

						//★クロマキーは抜くRGB(BGRかも)色, RGBからどれくらい離れた値まで抜くか, グレースケール変更時のmax, threshold関数を使用するときのtype
						//typeは以下参照。(ただし、指定色以外が抜かれないように調整してるので、大津メソッドの効果はないです。
						//http://labs.eecs.tottori-u.ac.jp/sd/Member/oyamada/OpenCV/html/py_tutorials/py_imgproc/py_thresholding/py_thresholding.html

						ChromakeyEffectRealtime chromakey = new ChromakeyEffectRealtime(colors, threshold, max, ttypeList[i]);
						settingList.add(new DataList(chromakey, dir, chromakey));					
					}
				}
			}
		}
	}

	public static String getStringFromType(int type) {
		//効果があるのはこの2つ
		if(type == THRESH_BINARY) {
			return "THRESH_BINARY";
		} else {
			return "THRESH_TOZERO";
		}
	}

	public void initColorkey(List<DataList> settingList, List<String> nameList) {

		for (int index = 0 ; index < effectList.length; index ++) {
			int ttypeList[] = {THRESH_BINARY,THRESH_TOZERO};
			int colors = 0;//black
			int threshold_step = 25;
			int threshold_start = 50;
			int threshold_max = 150;
			int threshold_max_start = 50;
			int threshold_max_step = 50;
			String dir = TOPDIR + "\\"+effectList[index]+".mp4";
	
			for (int i = 0 ; i < ttypeList.length ; i ++) {
				for (int max = threshold_max_start ; max < threshold_max ; max += threshold_max_step ) {
					for (int threshold = threshold_start ; threshold < threshold_max ; threshold += threshold_step) {
						String name = effectList[index] + "_colorkey_" + getStringFromType(ttypeList[i])+"_max_"+ max +"_threshold_"+threshold;
						nameList.add(name);

						//★カラーキーは抜く輝度を指定。クロマキーの指定色が輝度に変わっただけで、他は変わらず
						ColorkeyEffectRealtime chromakey = new ColorkeyEffectRealtime(colors, threshold, max, ttypeList[i]);
						settingList.add(new DataList(chromakey, dir, chromakey));					
					}
				}
			}
		}
	}

	public String getMaskPath(String name) {
		int index = mNameList.indexOf(name);
		return mSettingList.get(index).mMaskFile;
	}

	public EffectMask getImageMask(String name) {
		int index = mNameList.indexOf(name);
		return mSettingList.get(index).mMaskIF;
	}
}
