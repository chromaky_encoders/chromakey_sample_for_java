package encoder.encode.domain.resource;

import java.util.List;

import encoder.encode.domain.EffectMask;
import encoder.encode.domain.EffectOverlayAudio;
import encoder.encode.domain.EffectOverlayImage;

/**
 * Created by htsuji on 2017/12/13.
 * エフェクト関連情報を管理するためのクラス
 * domainのモジュールが使用することを想定
 * 使い勝手を考慮してstatic定義
 *  staticメソッドのinterface定義ができないので、implements EffectResourceInterface定義はしていない
 */
public class EffectResourceRepository {

	public static EffectResourceRepositoryImpl sInstance;

	//getInstanceはpublicにしているが基本使用しない
	public static EffectResourceInterface getInstance() {
		if(sInstance==null)
			sInstance = new EffectResourceRepositoryImpl();
		return sInstance;
	}

	public static List<String> getAll() {
		return getInstance().getAll();
	}

	public static String getPath(String name) {
		return getInstance().getPath(name);
	}

	public static EffectOverlayImage getOverlayImage(String name) {
		return getInstance().getOverlayImage(name);
	}

	public static EffectOverlayAudio getOverlayAudio(String name) {
		return getInstance().getOverlayAudio(name);
	}

	public static String getMaskPath(String name) {
		return getInstance().getMaskPath(name);
	}

	public static EffectMask getImageMask(String name) {
		return getInstance().getImageMask(name);
	}
}
