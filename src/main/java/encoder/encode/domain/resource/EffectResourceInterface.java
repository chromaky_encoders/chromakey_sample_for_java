package encoder.encode.domain.resource;

import java.util.List;

import encoder.encode.domain.EffectMask;
import encoder.encode.domain.EffectOverlayAudio;
import encoder.encode.domain.EffectOverlayImage;
/**
 * Created by htsuji on 2017/12/13.
 * エフェクト関連情報を取得するためのIF
 * domainのモジュールが使用することを想定
 */
public interface EffectResourceInterface {
	/**
     *  エフェクト一覧取得
     *  @return エフェクト名一覧
     */
	public List<String> getAll();

	/**
     *  エフェクトパス取得
     *  @param エフェクト名
     *  @return パス
     */
	public String getPath(String name);

	/**
     *  エフェクトイメージクラス取得
     *  @param エフェクト名
     *  @return エフェクトイメージクラス
     *  @note イメージがない場合はnullが返る
     */
	public EffectOverlayImage getOverlayImage(String name);

	/**
     *  エフェクト音声クラス取得
     *  @param エフェクト名
     *  @return エフェクト音声クラス
     *  @note 音声がない場合はnullが返る
     */
	public EffectOverlayAudio getOverlayAudio(String name);

	/**
     *  エフェクトイメージのマスクパス取得。
     *  @param エフェクト名
     *  @return エフェクトのマスクファイルパス
     *  @note パスがない場合はnullが返る。getMaskPath、getImageMaskともにnullの場合はmaskを利用しない
     */
	public String getMaskPath(String name);

	/**
     *  エフェクトイメージのマスクIF取得。エフェクト作成時のみ使用する予定
     *  @param エフェクト名
     *  @return エフェクトイメージのマスクIFクラス
     *  @note IFがない場合はnullが返る。Android上は常にnullかな
     */
	public EffectMask getImageMask(String name);
}
