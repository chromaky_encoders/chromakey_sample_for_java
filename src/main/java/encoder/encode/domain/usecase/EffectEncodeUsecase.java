package encoder.encode.domain.usecase;

import org.bytedeco.javacv.Frame;

/**
 * Created by htsuji on 2017/12/11.
 * エフェクトエンコード用IF定義
 * presentationのような上位モジュールが使用することを想定
 * videoだけ、audioだけの指定も出来るようにしておきたい
 */
public interface EffectEncodeUsecase {

    /**
     *  エンコードファイルロード開始
     *  @note ロード失敗時にException発生
     */
	public void load() throws Exception;

    /**
     *  エンコードファイル解放
     *  @note リリース失敗時にException発生
     */
    public void release() throws Exception;

	/**
     *  エンコード開始, まとめて実行したい場合
     *  @param srcFrame 入力フレーム
     *  @param x 横位置
     *  @param y 縦位置
     *  @param width 幅
     *  @param height 高さ
     *  
     *  @return 出力フレーム
     *  @note 音声に対してはエフェクトの次の音声を、動画に対しては動画を反映する。
     *  @note 1フレーム毎なのでここはblockingでよい
     *        もう一段動画保存用/カメラ描写用のクラスを用意して、そのクラスがEffectEncodeUsecaseを利用する
     */
    public Frame encodeFrame(Frame srcFrame, int x, int y, int width, int height) throws Exception;

	/**
     *  effectのvideo側読み込みが終わりまで行ったかどうか
     *  @return true:終わりまで, false:まだ読むフレームがある
     */
    public boolean isAtVideoEndpoint();

	/**
     *  effectのaudio側読み込みが終わりまで行ったかどうか
     *  @return true:終わりまで, false:まだ読むフレームがある
     */
    public boolean isAtAudioEndpoint();

    /**
     *  エンコードしたフレーム解放
     *  @param frame encodeFrameで取得したフレーム
     *  @note リークするのでencodeFrame後は必ず呼ぶこと
     */
    public void releaseFrame(Frame frame);

    /**
     *  エンコードファイル取得、のちにサムネを得られるように
     *  @return ファイルパス
     */
    public String getEffectFile();

    //ここから下は細かな制御が出来るようにするために外だししてるが、使わないならprivateに変更する。
    //映像生成用にはいるが、、
	/**
     *  エフェクト動画の次フレームを取得
     *  @return 次フレーム
     *  @note このフレームはreleaseImageFrameで破棄する
     */
	public EffectImageFrame getNextEffectImageFrame() throws Exception;

    /**
     *  エンコードしたフレーム解放
     *  @param frame encodeFrameで取得したフレーム
     *  @note リークするのでencodeFrame後は必ず呼ぶこと
     */
    public void releaseImageFrame(EffectImageFrame frame);
	/**
     *  エフェクト動画の次フレームを取得
     *  @return 次フレーム
     *  @note このフレームは破棄する必要なし
     */
	public Frame getNextEffectAudioFrame() throws Exception;

	/**
     *  イメージの合成エンコード開始
     *  @param srcFrame 入力フレーム
     *  @param effectFrame getNextEffectFrameで取得したエフェクトフレーム
     *  @param x 横位置
     *  @param y 縦位置
     *  @param width 幅
     *  @param height 高さ
     *  
     *  @return 出力フレーム
     *  @note 出力フレームはreleaseFrameで破棄すること
     */
	public Frame overlayImage(Frame srcFrame, EffectImageFrame effectFrame, int x, int y, int width, int height);

	/**
     *  音声の合成エンコード開始
     *  @param srcFrame 入力フレーム
     *  @param effectFrame getNextEffectFrameで取得したエフェクトフレーム
     *  
     *  @return 出力フレーム
     *  @note 出力フレームはreleaseFrameで破棄すること
     */
	public Frame overlayAudio(Frame srcFrame, Frame effectFrame);
}
