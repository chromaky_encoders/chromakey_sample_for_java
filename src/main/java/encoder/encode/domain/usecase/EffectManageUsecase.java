package encoder.encode.domain.usecase;

import java.util.List;

/**
 * Created by htsuji on 2017/12/13.
 * エフェクト利用のための情報取得IF定義
 */
public interface EffectManageUsecase {

    /**
     *  エフェクト一覧取得
     *  @return エフェクト名一覧
     */
	public List<String> getAll();

	/**
     *  エフェクトエンコーダ取得
     *  @param name エフェクト名
     *  @return エンコーダ
     */
	public EffectEncodeUsecase getEncoder(String name);

	/**
     *  動画へのエフェクト適用IF取得
     *  動画ファイルに対してはこれで十分。
     *  カメラからの映像には使えない
     *  @return 動画へのエフェクト適用IF(EffectEncodeUsecaseと利用して)
     */
	public EffectAppliedMovieUsecase getUsecase(String inputFile, String effectName) throws Exception;

}
