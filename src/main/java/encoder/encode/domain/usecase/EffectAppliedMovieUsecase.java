package encoder.encode.domain.usecase;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;

/**
 * Created by htsuji on 2017/12/24.
 * 保存動画にエフェクトを反映するためのIF定義。EffectEncodeUsecaseをラップする。
 * initialize時にinputファイル指定していることを明示したいのでabstractで定義
 * presentationのような上位モジュールが使用することを想定
 */
public abstract class EffectAppliedMovieUsecase {

	/**
     *  初期化時にエンコードファイル指定
     *  @note エンコードファイルオープン失敗時にException発生
     */
	public EffectAppliedMovieUsecase(String inputFile, EffectEncodeUsecase encoder) throws Exception{}

	/**
     *  エンコードファイル解放もしくはキャンセル。
     *  onError、onFinishedが発生した際も呼んでください
     *  @note リリース失敗時にException発生
     */
    public abstract void release() throws Exception;

    /**
     *  エンコード開始
     *  @param startFrameNo エフェクト適用開始フレーム番号(No)
     *  @param x 横位置
     *  @param y 縦位置
     *  @param width 幅
     *  @param height 高さ
     *  @param progressCallback 動画適用中の状態通知
     *  
     *  @note ロード失敗時にException発生
     */
	public abstract void start(int startFrameNo, int x, int y, int width, int height, EffectAppliedProgress progressCallback);

    /**
     *  元動画のgrabberを取得。映像サイズ等設定を見るために使う
     *  @return 元動画のgrabber. grabber.getImageWidth()等の元動画サイズ情報を取得するために利用する
     *  
     *  @note ロード失敗時にException発生
     */
	public abstract FFmpegFrameGrabber getSrcGrabber();

	/**
	 * Created by htsuji on 2017/12/24.
	 * start後に呼ばれるコールバック定義。start後の動作はthreadで実現する予定
	 * フレーム合成後の処理は呼ぶ側で実装
	 */
	public interface EffectAppliedProgress{
	    /**
	     *  マージ開始
	     */
		void onStart();
	    /**
	     *  マージした各フレーム通知。解放は必要なし
	     */
		void onFrame(Frame frame);
	    /**
	     *  マージ終了通知。onError発生時は呼ばない
	     */
		void onFinished();
	    /**
	     *  エラー時に通知
	     */
		void onError();
	    /**
	     *  途中キャンセル時通知。onError発生と合わせて呼ばれる可能性あり
	     */
		void onCanceled();
	}
}
