package encoder.encode.domain.usecase;

import java.util.List;

import encoder.encode.domain.EffectManageUsecaseImpl;

/**
 * Created by htsuji on 2017/12/13.
 * エフェクト利用のための情報取得IF処理定義。
 * 使い勝手を考慮してstatic定義
 * staticメソッドのinterface定義ができないので、implements EffectManageUsecaseはしていない
 * 使い方は同じ
 */
public class EffectManage {

	//扱いやすいようstaticで定義。実体はdomain内に
	public static EffectManageUsecaseImpl sInstance=null;

	//getInstanceはpublicにしているが基本使用しない
	public static EffectManageUsecase getInstance() {
		if(sInstance==null)
			sInstance = new EffectManageUsecaseImpl();
		return sInstance;
	}

	public static List<String> getAll() {
		return getInstance().getAll();
	}

	public static EffectEncodeUsecase getEncoder(String name) {
		return getInstance().getEncoder(name);
	}

	public static EffectAppliedMovieUsecase getUsecase(String inputFile, String effectName) throws Exception {
		return getInstance().getUsecase(inputFile, effectName);
	}
}
