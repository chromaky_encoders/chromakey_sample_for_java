package encoder.encode.domain.usecase;

import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacv.Frame;

/**
 * Created by htsuji on 2017/12/27.
 * エフェクト画像フレームデータ定義
 * EffectEncodeUsecase内部で使用されることを想定
 */
public class EffectImageFrame {
	/**
     *  画像フレーム
     */
	public Frame mFrame;

	/**
     *  抜き出しに利用するマスク画像(Mat)
     */
	public Mat mMasked;
}
