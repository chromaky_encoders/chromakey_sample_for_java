package encoder.encode.domain;

import java.nio.ShortBuffer;

import org.bytedeco.javacv.Frame;

/**
 * Created by htsuji on 2017/12/11.
 * エフェクト合成処理用インターフェイスクラス
 * 音声のみ
 */
public class EffectOverlayAudio {

	//メンバー変数
	//元動画のボリューム
	private double mSrcVolume;

	public EffectOverlayAudio() {
		//とりあえずハーフ&&ハーフで合成
		mSrcVolume = 0.5;
	}

	/***
     * フレーム重ね合わせ処理
     * @param srcFrame         元フレーム
     * @param effectFrame      重ねるフレーム
     * @return 重ね合わせたフレーム
     * @note 元のFrameデータは触らない
     */
	public Frame overlay(Frame srcFrame, Frame effectFrame) {
		Frame distFrame = srcFrame.clone();

		if( effectFrame == null ) return distFrame;

		short [] vals = null;
		for( int i = 0 ; i < distFrame.samples.length; i++ ) {
//			ShortBuffer buffer = (ShortBuffer)distFrame.samples[i];
			ShortBuffer srcBuffer = (ShortBuffer)srcFrame.samples[i];
			ShortBuffer effectBuffer = (ShortBuffer)effectFrame.samples[i];
			if(vals == null) vals = new short[srcBuffer.capacity()];
			//音声マージ
			for( int j = 0 ; j < srcBuffer.capacity(); j ++ ) {
				vals[j] = (short)((double)(srcBuffer.get(j)) * mSrcVolume + ((double)effectBuffer.get(j)) * (1.0 - mSrcVolume));
			}
			distFrame.samples[i] = ShortBuffer.wrap(vals);
		}
		return distFrame;
	}
}
