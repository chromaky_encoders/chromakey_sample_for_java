package encoder.encode.presentation;
import org.bytedeco.javacpp.opencv_core.Mat;

import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;

import java.io.File;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter.ToMat;

import encoder.encode.domain.usecase.EffectEncodeUsecase;
import encoder.encode.domain.usecase.EffectManage;

/*
 * 特定の背景をバックにしたエフェクト合成絵を作るサンプル
 * */
public class MaskExporter {
	private Mat mBackImage;
	private String mOutDir;
	private ToMat mToMat;
	private EffectEncodeUsecase mEncoder;

	public MaskExporter(String backImageName, String outDir, String effectName) {
		mBackImage = imread(backImageName);
		mOutDir = outDir;
		mToMat = new ToMat();

		//ディレクトリ作成
		File newfile = new File(outDir);
		newfile.mkdirs();

		mEncoder = EffectManage.getEncoder(effectName);
	}

	public void doEffect(int x, int y, int width, int height) {
		Frame frame;
		int index=0;

		try {
			mEncoder.load();

			while ( (frame = mEncoder.encodeFrame(mToMat.convert(mBackImage), x, y, width, height)) != null) {
				//write to file
				imwrite(mOutDir + "\\"+index +".png", mToMat.convert(frame));
				mEncoder.releaseFrame(frame);
				index++;
			}
			mEncoder.release();
		} catch (Exception e) {
			System.out.println("encodeFrame enter.");
			e.printStackTrace();
		}
	}
}
