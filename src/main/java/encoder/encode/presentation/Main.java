package encoder.encode.presentation;

import java.util.Calendar;
import java.util.List;

import encoder.encode.domain.usecase.EffectManage;

public class Main {

	public static final String TOPDIR="D:\\data\\testdata\\win\\effect_test\\320x240";
	public static final String BACKFILE=TOPDIR+"\\green.png";
	public static final int x = 300;
	public static final int y = 480;
	public static final int width = 320;
	public static final int height = 240;

	public static void main(String[] args) {
		Calendar now = Calendar.getInstance();
		String date = "Time" + now.get(Calendar.YEAR);
		date += now.get(Calendar.MONTH);
		date += now.get(Calendar.DATE);
		date += now.get(Calendar.HOUR_OF_DAY);
		date += now.get(Calendar.MINUTE);
		date += now.get(Calendar.SECOND);
		List<String> effectList = EffectManage.getAll();

		for(int i = 0 ; i < effectList.size();  i ++) {
			MaskExporter exporter = new MaskExporter(BACKFILE, TOPDIR+"\\"+date+"\\"+effectList.get(i), effectList.get(i));
			exporter.doEffect(x, y, width, height);
		}
	}

}
