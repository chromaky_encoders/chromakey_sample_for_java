package encoder.encode.presentation;

import java.util.Calendar;
import java.util.List;

import encoder.encode.domain.usecase.EffectManage;

public class ChromakeyMaskMain {
	public static final String TOPDIR="D:\\data\\testdata\\win\\effect_test\\chromakey";

	public static void main(String[] args) {
		Calendar now = Calendar.getInstance();
		String date = "Time" + now.get(Calendar.YEAR);
		date += now.get(Calendar.MONTH);
		date += now.get(Calendar.DATE);
		date += now.get(Calendar.HOUR_OF_DAY);
		date += now.get(Calendar.MINUTE);
		date += now.get(Calendar.SECOND);
		List<String> effectList = EffectManage.getAll();

		for(int i = 0 ; i < effectList.size();  i ++) {
			ExportChromakey exporter = new ExportChromakey(effectList.get(i));
			exporter.doFile(TOPDIR+"\\"+date+"\\", effectList.get(i)+".mp4");
		}
	}
}
