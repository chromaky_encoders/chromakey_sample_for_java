package encoder.encode.presentation;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import encoder.encode.domain.usecase.EffectManage;

public class MovieRecordMain {

	public static final String TOPDIR="D:\\data\\testdata\\win\\movie_test";
	public static final String SRCFILE=TOPDIR+"\\sky.mp4";
	public static final int x = 100;
	public static final int y = 100;
	public static final int width = 320;
	public static final int height = 240;

	public static void main(String[] args) {
		Calendar now = Calendar.getInstance();
		String date = "Time" + now.get(Calendar.YEAR);
		date += now.get(Calendar.MONTH);
		date += now.get(Calendar.DATE);
		date += now.get(Calendar.HOUR_OF_DAY);
		date += now.get(Calendar.MINUTE);
		date += now.get(Calendar.SECOND);
		String dir = TOPDIR+"\\"+date+"\\";

		List<String> effectList = EffectManage.getAll();

		//ディレクトリ作成
		File newfile = new File(dir);
		newfile.mkdirs();

		//かたっぱしから動画を作っていく
		for(int i = 0 ; i < effectList.size();  i ++) {
//		for(int i = 0 ; i < 1;  i ++) {
			MovieMerger merger;
			String outFile = TOPDIR+"\\"+date+"\\with_"+effectList.get(i)+".mp4";
			try {
				merger = new MovieMerger(SRCFILE, outFile, effectList.get(i));
				//約2秒後から動画追加
				merger.doEffect(60, x, y, width, height);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("encodeEffect" + effectList.get(i) + " error, write file to " + outFile);
			}
		}
	}

}
