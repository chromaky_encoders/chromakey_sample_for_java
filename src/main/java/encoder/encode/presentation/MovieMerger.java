package encoder.encode.presentation;

import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.avutil;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.Frame;

import encoder.encode.domain.usecase.EffectAppliedMovieUsecase;
import encoder.encode.domain.usecase.EffectManage;

public class MovieMerger implements EffectAppliedMovieUsecase.EffectAppliedProgress {

	private FFmpegFrameRecorder mRecorder;
	private EffectAppliedMovieUsecase mAppliedMovie;
    private int mFrameNumber = 0;
    private boolean mStopped = true;

	public MovieMerger(String inFile, String outFile, String effectName) throws Exception{
		int quarity=23;
		double gop=1.5;//sec

		//エフェクト動画適用IF取得
		mAppliedMovie = EffectManage.getUsecase(inFile, effectName);

		//mRecorderの準備
		FFmpegFrameGrabber grabber = mAppliedMovie.getSrcGrabber();
		mRecorder = new FFmpegFrameRecorder(outFile, grabber.getImageWidth(), grabber.getImageHeight(), getAudioChannels());
		//video設定
		if( isVideo() ) {
			mRecorder.setFormat(grabber.getFormat());
			mRecorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
			mRecorder.setVideoQuality(quarity);
	        mRecorder.setPixelFormat(avutil.AV_PIX_FMT_YUV420P);
	        mRecorder.setVideoOption("preset", "ultrafast");
	        mRecorder.setFrameRate(grabber.getFrameRate());
	        mRecorder.setGopSize((int)(gop * grabber.getFrameRate()));
			//mRecorder.setVideoBitrate(grabber.getVideoBitrate()*5);
	        //mRecorder.setVideoBitrate(grabber.getVideoBitrate());
		}

		//audio設定
		if( isAudio() ) {
			//mRecorder.setAudioBitrate(grabber.getAudioBitrate());
			mRecorder.setAudioQuality(quarity);
			mRecorder.setAudioCodec(avcodec.AV_CODEC_ID_AAC);
			mRecorder.setSampleRate(grabber.getSampleRate());
		}
	}

	private boolean isVideo() {
		return mAppliedMovie.getSrcGrabber().getVideoStream() != -1;
	}

	private int getAudioChannels() {
		return mAppliedMovie.getSrcGrabber().getAudioChannels();
	}

	private boolean isAudio() {
		return getAudioChannels() != 0;
	}

	public void doEffect(int frameNo, int x, int y, int width, int height) {
		mAppliedMovie.start(frameNo, x, y, width, height, this);
		mStopped = true;
		while(mStopped) {
			try {
				//とりあえず動画保存終了まで待つ
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void closeRecorder() {
		try {
			if(mAppliedMovie != null)
				mAppliedMovie.release();

			if(mRecorder != null) {
				mRecorder.stop();
				mRecorder.release();
				mRecorder=null;
			}
		} catch(org.bytedeco.javacv.FrameRecorder.Exception e) {
			e.printStackTrace();
			//何かする
		} catch(Exception e) {
			e.printStackTrace();
			//何かする
		} finally {
			mStopped=false;
		}
	}

	@Override
	public void onStart() {
		try {
			mRecorder.start();
			mRecorder.setFrameNumber(0);
		} catch (org.bytedeco.javacv.FrameRecorder.Exception e) {
			e.printStackTrace();
			//何かする
		}		
	}

	@Override
	public void onFrame(Frame frame) {
		try {
			if(frame.image != null) {//画像の場合はframeカウントアップしつつ保存
				mRecorder.setFrameNumber(mFrameNumber++);
				mRecorder.record(frame);
			} else if(frame.samples != null) {//音声はそのまま保存
				mRecorder.recordSamples(frame.samples);
			} else {
				//fail safe, 何もしない
			}
		} catch (org.bytedeco.javacv.FrameRecorder.Exception e) {
			e.printStackTrace();
			//何かする
			System.exit(0);
		}
	}

	@Override
	public void onFinished() {
		closeRecorder();
	}

	@Override
	public void onError() {
		closeRecorder();
	}

	@Override
	public void onCanceled() {
		//キャンセルはしないので特に何も
		System.out.println("encodeFrame enter.");
	}

}
