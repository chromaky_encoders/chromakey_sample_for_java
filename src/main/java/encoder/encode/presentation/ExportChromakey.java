package encoder.encode.presentation;

import java.io.File;

import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.avutil;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.OpenCVFrameConverter.ToMat;

import encoder.encode.domain.usecase.EffectEncodeUsecase;
import encoder.encode.domain.usecase.EffectImageFrame;
import encoder.encode.domain.usecase.EffectManage;

public class ExportChromakey {
	private EffectEncodeUsecase mEncoder;

	public ExportChromakey(String effectName){
		//エフェクト動画適用IF取得
		mEncoder = EffectManage.getEncoder(effectName);
	}

	public void doFile(String outDir, String outFile) {
		int quarity=23;
		int frate=30;
		int fnum=0;
		double gop=1;//sec
		//ディレクトリ作成
		File newfile = new File(outDir);
		newfile.mkdirs();

		FFmpegFrameRecorder recorder=null;
		EffectImageFrame frame;
		ToMat toMat = new ToMat();
		try {
			mEncoder.load();

			while ( (frame = mEncoder.getNextEffectImageFrame()) != null) {
				if(frame.mMasked == null) {
					break;
				}
				//write to file
				if(recorder == null) {
					recorder = new FFmpegFrameRecorder(outDir + outFile, frame.mMasked.cols(), frame.mMasked.rows(), 0);
					//video設定
					recorder.setFormat("mp4");
					recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
					recorder.setVideoQuality(quarity);
					recorder.setPixelFormat(avutil.AV_PIX_FMT_YUV420P);
					recorder.setVideoOption("preset", "ultrafast");
					recorder.setFrameRate(frate);
					recorder.setGopSize((int)(gop * frate));
					recorder.start();
				}
				recorder.setFrameNumber(fnum++);
				recorder.record(toMat.convert(frame.mMasked));
				frame.mMasked.close();
			}
			mEncoder.release();
		} catch (Exception e) {
			System.out.println("encodeFrame enter.");
			e.printStackTrace();
		} finally {
			if(recorder != null) {
				try {
					recorder.stop();
					recorder.release();
				} catch(FFmpegFrameRecorder.Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

}
